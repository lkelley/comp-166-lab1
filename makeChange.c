#include <stdio.h>
#include <stdlib.h>
#include <math.h>


/**
 * Reads a dollar amount from stdin and returns it as whole cents
 * @return The read value in whole cents
 */
long readDollarInputToCents() {
    char sInput[256];
    double dInput;
    fgets(sInput, sizeof(sInput), stdin); // Read string input, dollars.cents
    dInput = strtod(sInput, NULL); // Convert input to double
    dInput = round(dInput * 100); // Convert to cents and round
    return (long)dInput; // Return as a long in the form of cents
}


int main() {
    long purchaseCents, changeCents, tenderedCents;

    printf("Enter purchase amount:"); // Prompt for purchase amount
    purchaseCents = readDollarInputToCents(); // Read in purchase price and store as cents

    printf("Enter the amount tendered:"); // Prompt for tendered amount
    tenderedCents = readDollarInputToCents(); // Read tendered amount

    changeCents = tenderedCents - purchaseCents; // Calculate change

    /*
     * Check that amounts are positive
     */
    if (tenderedCents < 0 || purchaseCents < 0) {
        puts("Amounts must be positive.\n");
        return EXIT_FAILURE;
    }

    /*
     * Check that tendered amount is sufficient
     */
    if (tenderedCents < purchaseCents) {
        printf("Must tender at least an additional $%.2f\n", (double)abs(changeCents)/100);
        return EXIT_FAILURE;
    }

    printf("Change due is %.2f\n", changeCents/100.0);

    long nickels = (long)round(changeCents / 5.0); // Number of nickels
    changeCents *= 5; // Store rounded change in cents
    printf("Rounded to the nearest nickel %.2f\n", (nickels * 0.05));

    printf("%ld Twenties\n", changeCents / 2000);
    changeCents = changeCents % 2000;

    printf("%ld Tens\n", changeCents / 1000);
    changeCents = changeCents % 1000;

    printf("%ld Fives\n", changeCents / 500);
    changeCents = changeCents % 500;

    printf("%ld Toonies\n", changeCents / 200);
    changeCents = changeCents % 200;

    printf("%ld Loonies\n", changeCents / 100);
    changeCents = changeCents % 100;

    printf("%ld Quarters\n", changeCents / 25);
    changeCents = changeCents % 25;

    printf("%ld Dimes\n", changeCents / 10);
    changeCents = changeCents % 10;

    printf("%ld Nickels\n", changeCents / 5);

    return EXIT_SUCCESS;
}
